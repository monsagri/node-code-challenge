const validateQueryLength = locationQuery => locationQuery.length >= 2;

const getLocationData = (locationQuery, db) =>
  new Promise((resolve, reject) => {
    db.all(
      `
    SELECT 
      name, geonameid, latitude, longitude 
    FROM 
      locations 
    WHERE 
      name LIKE '%${locationQuery}%' 
    ORDER BY 
      instr(name,'%${locationQuery}%') ASC,
      length(name) ASC
    `,
      [],
      (e, data) => {
        if (e) {
          console.error(`SQLite Error: ${e}`);
          reject(e);
        }
        resolve(data);
      }
    );
  });

module.exports = { validateQueryLength, getLocationData };
