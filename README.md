# Medicspot Dev Challenge

This is my implementation of the [Medicspot Dev Challenge](./BRIEF.md).  
The answers to the questions can be found in the [question list](./QUESTIONS.md).

The implementation of the challenge is quite simple, but it was an interesting challenge starting something from scratch and building on it.

## Installation / Running the code

- Clone the code to your machine
- Run the following commands from the project directory
- Install dependencies with `yarn` or `npm install`
- Start project with `yarn start` - this will build the frontend javascript and start the server
- Go to localhost:3000 to view the frontend

## Challenges / Improvements

### Backend

I had not worked with SQLite before, and while I've worked with SQL databases, the setup and import of the database itself was interesting. (I really should have opened that hints file)  
Sorting by the closest name match was a bit of a challenge since I couldn't find a match rating, but I decided to improvise a bit and at least sort by the shortest result, with the search term being as early as possible in the result.

### Frontend

I chose to use frontend framework I haven't worked with before but heard of in ANT and that was quite fun. I would usually separate my React code out into components, but with how basic the implementation is, I don't think there are any natural points to do so.

### Testing

I seem to have hit the recommended time spent on the challenge, but I would add some basic tests for the input validation and output format.
