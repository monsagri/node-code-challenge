// Server code goes here
const express = require("express");
const sqlite3 = require("sqlite3").verbose();
const cors = require("cors");
const { getLocationData, validateQueryLength } = require("./services");

const app = express();

app.use(cors());

app.use(express.json());

const port = 3000; // move this to env

const db = new sqlite3.Database("./data/locations.db", err => {
  if (err) {
    console.error(`Error connecting to the DB${err.message}`);
  }
  console.log("DB connected.");
});

app.get("/locations", async ({ query: { q: locationQuery } }, res) => {
  if (!validateQueryLength(locationQuery))
    return res
      .status(400)
      .send("Please provide at least two characters in your query.");

  try {
    const result = await getLocationData(locationQuery, db);
    return res.status(200).json(result);
  } catch (e) {
    return res.status(500).json(e);
  }
});

app.use(express.static(`${__dirname}/public`));
app.get("/", (req, res) => res.sendFile(`${__dirname}/public/index.html`));

app.listen(port, () => console.log(`Server up and running on port ${port}`));
