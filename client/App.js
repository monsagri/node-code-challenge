import React, { useState } from "react";
import axios from "axios";
import { Input, List } from "antd";
import "antd/dist/antd.css";

export default () => {
  const [searchTimeout, setSearchTimeout] = useState();
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(false);

  return (
    <div style={{ padding: "0 20" }}>
      <h1>Locations of interest - UK</h1>
      <Input
        placeholder="Search for locations"
        onChange={({ target: { value } }) => {
          clearTimeout(searchTimeout);
          if (value.length >= 2) {
            setLoading(true);
            return setSearchTimeout(
              setTimeout(async () => {
                const result = await axios.get(
                  `http://localhost:3000/locations?q=${value}`
                );
                setLoading(false);
                setSearchResults(result.data);
              }, 500)
            );
          }
          setSearchResults([]);
        }}
        style={{ width: 200, height: 20 }}
      />
      <List
        itemLayout="horizontal"
        dataSource={searchResults}
        header="Results"
        loading={loading}
        pagination={{ pageSize: 10 }}
        renderItem={({ name, geonameid, latitude, longitude }) => (
          <List.Item key={geonameid}>
            <List.Item.Meta
              title={name}
              description={`Lat: ${latitude}, Long: ${longitude}`}
            />
          </List.Item>
        )}
      />
    </div>
  );
};
