const { validateQueryLength } = require("./services");

describe("Validation tests", () => {
  console.log("checking validation logic");
  {
    test("empty input should return false", () => {
      expect(validateQueryLength("a")).toBe(false);
    });

    test("valid input should return true", () => {
      expect(validateQueryLength("london")).toBe(true);
    });
  }
});

describe("SQL Query should return valid data", () => {
  // Some mocking for sqlite and data here
});
