# Questions

Qs1: Explain the output of the following code and why

```js
setTimeout(function() {
  console.log("1");
}, 100);
console.log("2");
```

### Answer

The code will output 2, followed by one after 100ms. `setTimeout` will delay the exection of the first `console.log` by 100ms.

Qs2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

### Answer

This function will output a countdown from 10 to 0.  
We will recursively call `foo` with the incremented input until we hit `d === 10`, at which point the console logs will be executed.

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

### Answer

The main issue I see with this code is that the `OR` operator will coerce the provided values to truthy/falsey and 0 is falsey in Javascript.  
This means an input of 0 (which seems like a valid numerical input) will default to 5.

Qs4: Explain the output of the following code and why

```js
function foo(a) {
  return function(b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

### Answer

We are using currying in foo (a function returning another function), which means that `bar` will result in a function that adds 1 to its input and the final output will be 3.

Qs5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function() {
    done(a * 2);
  }, 100);
}
```

### Answer

In this case, `double` is a function that will return the result of invoking `done` with a param of `a` \* 2 after 100ms.  
We might have a usecase in which we need to wait 100ms for some external function to complete, in which case we could use `double` to delaye calling our `done` function.
